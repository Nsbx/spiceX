import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { SpiceProvider } from "../../providers/spice/spice";
import { CapsuleDetails } from '../capsule-details/capsule-details';

@Component({
	selector: 'capsules-list',
	templateUrl: 'capsules-list.html'
})

export class CapsulesList {
	capsules = new Array<any>();
	filter = new Array<any>();

	constructor(public navCtrl: NavController, private spiceProvider: SpiceProvider, public navParams: NavParams) {
		this.updateData();
	}

	getCapsuleDetails(capsule) {
		this.navCtrl.push(CapsuleDetails, {
			capsule: capsule
		});
	}

	resetFilter() {
		this.filter = this.capsules;
	}

	getFilter(event: any) {
		this.resetFilter();
		const val = event.target.value;
		if (val && val.trim() != '') {
			this.filter = this.filter.filter((item) => {
				return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
			})
		}
	}

	updateData() {
		this.spiceProvider.getAllCapsules().subscribe(data => {
			this.capsules = data;
			this.filter = data;
		})
	}
}
