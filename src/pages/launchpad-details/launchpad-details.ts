import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
	selector: 'launchpad-details',
	templateUrl: 'launchpad-details.html'
})

export class LaunchpadDetails {
	
	launchpad: any;

	constructor(public navCtrl: NavController, public navParams: NavParams) {
		this.launchpad = navParams.get('launchpad');
	}
}
