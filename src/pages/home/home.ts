import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SpiceProvider } from "../../providers/spice/spice";
import { CompanyHistory } from '../company-history/company-history';

@Component({
	selector: 'page-home',
	templateUrl: 'home.html'
})
export class HomePage {

	companyInfo = new Array<any>();

	constructor(public navCtrl: NavController, private spiceProvider: SpiceProvider) {
		this.spiceProvider.getCompanyInfo().subscribe(data => {
			this.companyInfo = data;
		})
	}

	getCompanyHistory() {
		this.navCtrl.push(CompanyHistory, {});
	}

}
