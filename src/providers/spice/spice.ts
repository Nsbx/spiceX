import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from "rxjs/Rx";
/*
Generated class for the SpiceProvider provider.

See https://angular.io/guide/dependency-injection for more info on providers
and Angular DI.
*/
@Injectable()
export class SpiceProvider {
	private baseUrl: string = "https://api.spacexdata.com/v2";
	
	constructor(public http: HttpClient) {
		console.log('Hello SpiceProvider Provider');
	}
	
	getCompanyInfo(): Observable<any> {
		return this.http.get(`${this.baseUrl}/info`);
	}

	/*
	* start       		Filtrer par date 				2013-12-03
	* end         		Filtrer par date 				2013-12-03
	* flight_number  	Filtrer par numéro de vol  		5
	* order            	Changer l'ordre					asc, desc
	*/
	getCompanyHistory(params): Observable<any> {
		params.start == undefined ? params.start = '' : true;
		params.end == undefined ? params.end = '' : true;
		params.flight_number == undefined ? params.flight_number = '' : true;
		params.order == undefined ? params.order = '' : true;
		let url = `${this.baseUrl}/info/history`;
		url += "?start=" + params.start;
		url += "&end=" + params.end;
		url += "&flight_number=" + params.flight_number;
		url += "&order=" + params.order;
		return this.http.get(url);
	}

	getLatestLaunch(): Observable<any> {
		return this.http.get(`${this.baseUrl}/launches/latest`);
	}

	getNextLaunch(): Observable<any> {
		return this.http.get(`${this.baseUrl}/launches/next`);
	}

	getAllPastLaunches(): Observable<any> {
		return this.http.get(`${this.baseUrl}/launches`);
	}

	getAllUpcomingLaunches(): Observable<any> {
		return this.http.get(`${this.baseUrl}/launches/upcoming`);
	}

	getAllUpcomingAndPastLaunches(): Observable<any> {
		return this.http.get(`${this.baseUrl}/launches/all`);
	}

	/*
	* id					Set sur true pour avoir les id database			true, false
	* flight_id				Filtrer par id database							1
	* order					Changer l'ordre									asc, desc
	* start					Filtrer par date 								2013-12-03
	* final					Filtrer par date 								2013-12-03
	* flight_number			Filtrer par numéro de vol						5
	* launch_year			Filtrer par année								2018
	* launch_date_utc		Filtrer par timestamp UTC						2010-06-04T18:45:00Z
	* launch_date_local		Filtrer timestamp ISO local						2010-06-04T14:45:00-04:00
	* rocket_id				Filtrer par rocket id							falcon1
	* rocket_name			Filtrer par rocket name							Falcon 1
	* rocket_type			Filtrer par rocket type							Merlin C
	* core_serial			Filtrer par core serial #						Merlin3C
	* cap_serial			Filtrer par dragon capsule serial #				C101
	* core_flight			Filtrer par core flight number					1
	* block					Filtrer par core block number					1
	* core_reuse			Filtrer par core reusability					true, false
	* side_core1_reuse		Filtrer par Falcon Heavy side core 1 reuse		true, false
	* side_core2_reuse		Filtrer par Falcon Heavy side core 2 reuse		true, false
	* fairings_reuse		Filtrer par fairing reuse						true, false
	* capsule_reuse			Filtrer par dragon capsule reuse				true, false
	* site_id				Filtrer par launch site id						ccafs_slc_40
	* site_name				Filtrer par launch site name					CCAFS SLC 40
	* site_name_long		Filtrer par long launch site name				Cape Canaveral Air Force Station Space Launch Complex 40
	* payload_id			Filtrer par payload id							COTS Demo Flight 2
	* customer				Filtrer par launch customer						NASA(COTS)
	* payload_type			Filtrer par payload type						Dragon 1.0
	* orbit					Filtrer par payload orbit						LEO
	* launch_success		Filtrer par successful launches					true, false
	* reused				Filtrer par launches with reused cores			true, false
	* land_success			Filtrer par sucessful core landings				true, false
	* landing_type			Filtrer par landing method						Ocean
	* landing_vehicle		Filtrer par landing vehicle						JRTI
	*/
	getLaunches(params): Observable<any> {
		params.id == undefined ? params.id = '' : true;
		params.flight_id  == null ? params.flight_id  = '' : true;
		params.order  == null ? params.order  = '' : true;
		params.start  == null ? params.start  = '' : true;
		params.final  == null ? params.final  = '' : true;
		params.flight_number  == null ? params.flight_number  = '' : true;
		params.launch_year  == null ? params.launch_year  = '' : true;
		params.launch_date_utc  == null ? params.launch_date_utc  = '' : true;
		params.launch_date_local  == null ? params.launch_date_local  = '' : true;
		params.rocket_id  == null ? params.rocket_id  = '' : true;
		params.rocket_name  == null ? params.rocket_name  = '' : true;
		params.rocket_type  == null ? params.rocket_type  = '' : true;
		params.core_serial  == null ? params.core_serial  = '' : true;
		params.cap_serial  == null ? params.cap_serial  = '' : true;
		params.core_flight  == null ? params.core_flight  = '' : true;
		params.block  == null ? params.block  = '' : true;
		params.core_reuse  == null ? params.core_reuse  = '' : true;
		params.side_core1_reuse  == null ? params.side_core1_reuse  = '' : true;
		params.side_core2_reuse  == null ? params.side_core2_reuse  = '' : true;
		params.fairings_reuse  == null ? params.fairings_reuse  = '' : true;
		params.capsule_reuse  == null ? params.capsule_reuse  = '' : true;
		params.site_id  == null ? params.site_id  = '' : true;
		params.site_name  == null ? params.site_name  = '' : true;
		params.site_name_long  == null ? params.site_name_long  = '' : true;
		params.payload_id  == null ? params.payload_id  = '' : true;
		params.customer  == null ? params.customer  = '' : true;
		params.payload_type  == null ? params.payload_type  = '' : true;
		params.orbit  == null ? params.orbit  = '' : true;
		params.launch_success  == null ? params.launch_success  = '' : true;
		params.reused  == null ? params.reused  = '' : true;
		params.land_success  == null ? params.land_success  = '' : true;
		params.landing_type  == null ? params.landing_type  = '' : true;
		params.landing_vehicle == null ? params.landing_vehicle = '' : true;
		let url = `${this.baseUrl}/launches`;
		url += "?id=" + params.id;
		url += "&flight_id=" + params.flight_id;
		url += "&order=" + params.order;
		url += "&start=" + params.start;
		url += "&final=" + params.final;
		url += "&flight_number=" + params.flight_number;
		url += "&launch_year=" + params.launch_year;
		url += "&launch_date_utc=" + params.launch_date_utc;
		url += "&launch_date_local=" + params.launch_date_local;
		url += "&rocket_id=" + params.rocket_id;
		url += "&rocket_name=" + params.rocket_name;
		url += "&rocket_type=" + params.rocket_type;
		url += "&core_serial=" + params.core_serial;
		url += "&cap_serial=" + params.cap_serial;
		url += "&core_flight=" + params.core_flight;
		url += "&block=" + params.block;
		url += "&core_reuse=" + params.core_reuse;
		url += "&side_core1_reuse=" + params.side_core1_reuse;
		url += "&side_core2_reuse=" + params.side_core2_reuse;
		url += "&fairings_reuse=" + params.fairings_reuse;
		url += "&capsule_reuse=" + params.capsule_reuse;
		url += "&site_id=" + params.site_id;
		url += "&site_name=" + params.site_name;
		url += "&site_name_long=" + params.site_name_long;
		url += "&payload_id=" + params.payload_id;
		url += "&customer=" + params.customer;
		url += "&payload_type=" + params.payload_type;
		url += "&orbit=" + params.orbit;
		url += "&launch_success=" + params.launch_success;
		url += "&reused=" + params.reused;
		url += "&land_success=" + params.land_success;
		url += "&landing_type=" + params.landing_type;
		url += "&landing_vehicle=" + params.landing_vehicle;
		return this.http.get(url);
	}

	getAllRockets(): Observable<any> {
		return this.http.get(`${this.baseUrl}/rockets`);
	}

	getRocket(params = {rocket_id : ''}): Observable<any> {
		let url = `${this.baseUrl}/rockets`;
		url += "/" + params.rocket_id;
		return this.http.get(url);
	}

	getAllCapsules(): Observable<any> {
		return this.http.get(`${this.baseUrl}/capsules`);
	}

	getCapsule(params = {capsule_id : ''}): Observable<any> {
		let url = `${this.baseUrl}/capsules`;
		url += "/" + params.capsule_id;
		return this.http.get(url);
	}

	getAllLaunchpads(): Observable<any> {
		return this.http.get(`${this.baseUrl}/launchpads`);
	}

	getLaunchpad (params = {launchpad_id : ''}): Observable<any> {
		let url = `${this.baseUrl}/launchpads`;
		url += "/" + params.launchpad_id;
		return this.http.get(url);
	}

	getDetailledInfoForAllCapsules(): Observable<any> {
		return this.http.get(`${this.baseUrl}/parts/caps`);
	}

	/*
	* capsule_serial		Filter by capsule serial #				C201
	* capsule_id			Filter by capsule id					dragon2
	* status				Filter by capsule status				active
	* original_launch		Filter by original launch date			2014-04-18T19:25:00Z
	* missions				Filter by flight missions				SpaceX CRS-3
	* landings				Filter by # of landings					1
	* type					Filter by type of dragon capsule		Dragon 1.1
	*/
	getDetailledInfoForCapsule(params): Observable<any> {
		params.id == undefined ? params.id = '' : true;
		params.capsule_serial == undefined ? params.capsule_serial = '' : true;
		params.capsule_id == undefined ? params.capsule_id = '' : true;
		params.status == undefined ? params.status = '' : true;
		params.original_launch == undefined ? params.original_launch = '' : true;
		params.missions == undefined ? params.missions = '' : true;
		params.landings == undefined ? params.landings = '' : true;
		params.type == undefined ? params.type = '' : true;
		let url = `${this.baseUrl}/parts/caps`;
		url += "?capsule_serial=" + params.capsule_serial;
		url += "&capsule_id=" + params.capsule_id;
		url += "&status=" + params.status;
		url += "&original_launch=" + params.original_launch;
		url += "&missions=" + params.missions;
		url += "&landings=" + params.landings;
		url += "&type=" + params.type;
		return this.http.get(url);
	}

	getDetailledInfoForAllCores(): Observable<any> {
		return this.http.get(`${this.baseUrl}/parts/cores`);
	}

	/*
	* core_serial			Filter by core serial #					B1032
	* block					Filter by core block number				3
	* status				Filter by flight status					destroyed
	* original_launch		Filter by original launch date			2017-05-01T11:15:00Z
	* missions				Filter by flight missions				GovSat-1
	* rtls_attempt			Filter by at least 1 rtls attempt		true, false
	* rtls_landings			Filter by total number of landings		4
	* asds_attempt			Filter by at least 1 asds attempt		true, false
	* asds_landings			Filter by total number of landings		4
	* water_landing			Filter by at least 1 water landing		true, false
	*/
	getDetailledInfoForCore(params): Observable<any> {
		params.id == undefined ? params.id = '' : true;
		params.core_serial == undefined ? params.core_serial = '' : true;
		params.block == undefined ? params.block = '' : true;
		params.status == undefined ? params.status = '' : true;
		params.original_launch == undefined ? params.original_launch = '' : true;
		params.missions == undefined ? params.missions = '' : true;
		params.rtls_attempt == undefined ? params.rtls_attempt = '' : true;
		params.rtls_landings == undefined ? params.rtls_landings = '' : true;
		params.asds_attempt == undefined ? params.asds_attempt = '' : true;
		params.asds_landings == undefined ? params.asds_landings = '' : true;
		params.water_landing == undefined ? params.water_landing = '' : true;
		let url = `${this.baseUrl}/parts/cores`;
		url += "?core_serial=" + params.core_serial;
		url += "&block=" + params.block;
		url += "&status=" + params.status;
		url += "&original_launch=" + params.original_launch;
		url += "&missions=" + params.missions;
		url += "&rtls_attempt=" + params.rtls_attempt;
		url += "&rtls_landings=" + params.rtls_landings;
		url += "&asds_attempt=" + params.asds_attempt;
		url += "&asds_landings=" + params.asds_landings;
		url += "&water_landing=" + params.water_landing;
		return this.http.get(url);
	}
	
}
